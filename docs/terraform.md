# Terraform
This page has some terraform info that I seem to always forget

## tags
Using tags on your infra is very important and is done a lot, one technique I used to DRY is to use the
terraform merge function:

main.tf
```
resource "aws_s3_bucket" "backsplash" {
  bucket = "splunk-bucket"
  acl    = "private"

  tags - "${merge(var.common_tags, var.tags)}"
}
```

vars.tf
```
variable "tags" { type = "map" }

variable "common_tags" {
  type = "map"

  default = {
    ApplicationOwner = "infosec"
    SupportOwner     = "CloudOps"
  }
}
```

You then add values to the "tags" variable when you consume this module, the merge function will merge the maps together.
Another cool thing, is if you want to replace one of the values in the common_tags variable, you just define it in "tags"
and it will be overwritten (or so the docs say, haven't tested it)

## random_id
s3 buckets have unique names, therefore it's nice to append a randomly generatored # to the end, for this we use the random_id resource.
I also use the lower function because s3 buckets have to be lowercase, and the random_id will sometimes make the values in the string uppercase.

```
resource "random_id" "s3" {
  byte_length = 4
}

resource "aws_s3_bucket" "backsplash" {
  bucket = "splunk-kinesis-backsplash-${lower(random_id.s3.b64_url)}"
  acl    = "private"
}
```

## using a value in a map
You can use a value in a map by calling it via `${var.map["key"]}`
```
resource "aws_s3_bucket" "backsplash" {
  bucket = "${var.tags["Environment"]}-splunk-kinesis-backsplash-${lower(random_id.s3.b64_url)}"
  acl    = "private"
}
```
