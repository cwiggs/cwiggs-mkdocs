# mkdocs
documentation on the mkdocs program, the program used to run this docs site

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.


## Markdown
[markdown syntax](https://daringfireball.net/projects/markdown/syntax)

## Setting up material them
orignially I installed mkdocs with apt-get, however, when you do that, and then install mkdocs-material with pip, mkdocs can't find the mkdocs-material theme.
The material repo has that info here: https://squidfunk.github.io/mkdocs-material/getting-started/#troubleshooting
However they don't explain how to work around it.  Therefore I uninstalled mkdocs from apt-get and installed it with pip, however now it requires sudo, which I don't like.

## todo to get mkdocs working
* fix pip needing sudo, which then makes mkdocs need sudo
* upload git to bitbucket
* Setup function on GCP VM to auto-update git and website
